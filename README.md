![](https://elioway.gitlab.io/elioangels/sinsay/elio-sinsay-logo.png)

> Tell Yeoman what to say **the elioWay**

# sinsay ![release](https://elioway.gitlab.io/static/release.png "release")

Like [cowsay](https://en.wikipedia.org/wiki/Cowsay) but more skull.

- [sinsay Documentation](https://elioway.gitlab.io/elioangels/sinsay)

## Installing

```
npm install sinsay
or
yarn add sinsay
```

- [Installing sinsay](https://elioway.gitlab.io/elioangels/sinsay/installing.html)

## Seeing is Believing

```
npm  -g install sinsay
or
yarn add global sinsay
sinsay 'Fork of yosay'
```

## Nutshell

- [sinsay Quickstart](https://elioway.gitlab.io/elioangels/sinsay/quickstart.html)
- [sinsay Credits](https://elioway.gitlab.io/elioangels/sinsay/credits.html)

![](https://elioway.gitlab.io/elioangels/sinsay/apple-touch-icon.png)

## License

[BSD-2-Clause © Google](license) [Tim Bushell](mailto:tcbushell@gmail.com)
