# sinsay Quickstart

## Nutshell

1. Install and import into your javascript modules.
2. Call `sinsay`

## Usage

```javascript
const sinsay = require("sinsay")

console.log(
  sinsay(
    "Have you got any spare parts? I'm looking for legs. And arms. Thighs are good. Ribs? Especially a spine? Do you need yours? I already have a skull."
  )
)
```

Outputs:

```

    .-"------"-.     +--------------------------+
   /            \    |  Have you got any spare  |
  |              |   |  parts? I'm looking for  |
  |\  .-.  .-.  /|   |  legs. And arms. Thighs  |
  | )(__/  \__)( |   |      are good. Ribs?     |
  |/     /\     \|   |  Especially a spine? Do  |
  (_     ^^     _)   |     you need yours? I    |
   \__|IIIIII|__/    |   already have a skull.  |
    | \IIIIII/ |     +--------------------------+
    \          /
     `--------`
```

## CLI

```
npm install --global sinsay
```

Help on command.

```
sinsay --help
```

Usage on command.

```
sinsay <string>
sinsay <string> --maxLength 8
echo <string> | sinsay
```

Example:

```
sinsay 'Fork of yosay'
```

Outputs

```
  .-"------"-.
 /            \
|              |   +--------------------------+
|\  .-.  .-.  /|   |       Fork of yosay      |
| )(__/  \__)( |   +--------------------------+
|/     /\     \|
(_     ^^     _)
 \__|IIIIII|__/
  | \IIIIII/ |
  \          /
   `--------`
```
