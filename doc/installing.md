# Installing sinsay

## Prerequisites

- [elioWay Prerequisites](https://elioway.gitlab.io/installing.html)

## npm

Install into your SASS projects.

```
npm install @elioway/sinsay
yarn add @elioway/sinsay
```

Import the functions into your master scss file.

```
@import "@elioway/sinsay/dist/sinsay";
```

## Development

```bash
cd elioway
git clone https://gitlab.com/elioway/elioangels.git
cd elioangels
git clone https://gitlab.com/elioangels/sinsay.git
```
